import re
import urlparse

from django.core.urlresolvers import resolve, Resolver404

INCLUDE_TAG = re.compile(r'<!--#[\s.]+include[\s.]+virtual=["\'](?P<path>.+)["\'][\s.]+-->')


class FakesiMiddleware(object):
    def process_request(self, request):
        def nginx_ssi(request):
            return request.META.get('HTTP_NGINX_SSI', 'off') == 'on'

        def is_ssi(request):
            return request.GET.get('ssi', '0') == '1'

        request.is_nginx = lambda *a, **kwa: nginx_ssi(request, *a, **kwa)
        request.is_ssi = lambda *a, **kwa: is_ssi(request, *a, **kwa)

    def process_response(self, request, response):
        if request.is_nginx():
            return response
        else:
            def ssi_include(match):
                path = match.group('path')
                func, args, kwargs = resolve(path)
                up = urlparse.urlparse(path)

                request.META['PATH_INFO'] =\
                    request.path_info =\
                    request.path = up.path
                request.META['QUERY_STRING'] = up.query

                # TODO: Insert fancy response when path doesn't resolve.

                return func(request).content

            response.content = re.sub(INCLUDE_TAG, ssi_include, response.content)
            response['Content-Length'] = len(response.content)

            return response