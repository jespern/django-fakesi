from django.template import Library

register = Library()

@register.simple_tag
def ssi(url):
	return """<!--# include virtual="/%s?ssi=1" -->""" % url
