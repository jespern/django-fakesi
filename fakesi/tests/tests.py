from BeautifulSoup import BeautifulSoup as BS

from django.test import TestCase

from django.contrib.auth.models import User

from django.test.client import Client

class LolTest(TestCase):
	def test_me(self):
		c = Client()

		resp = c.get('/')

		soup = BS(resp.content)

		for usertag in soup.findAll(id="user"):
			assert usertag.string == 'AnonymousUser'


class TestLoggedIn(TestCase):
	def test_login(self):
		c = Client()

		# make a user
		User.objects.create_user('admin', 'admin@admin.ca', 'admin')

		assert c.login(username='admin', password='admin') == True, "couldn't log in"

		resp =  c.get('/')

		soup = BS(resp.content)

		for usertag in soup.findAll(id="user"):
			assert usertag.string == 'admin'