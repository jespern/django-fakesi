import sys
import random

from django.template import RequestContext

from django.shortcuts import render_to_response
from django.http import HttpResponse

def root(request):
    return render_to_response('templates/root.html', 
        {}, RequestContext(request))

def ssi(request):
    resp = render_to_response('templates/ssi.html',
                { 'random': random.randint(1, sys.maxint) }, 
                RequestContext(request))

    resp['SSI'] = '1'

    return resp