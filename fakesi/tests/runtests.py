#!/usr/bin/env python
import os
import sys

from django.conf import settings

if not settings.configured:
    settings.configure(
        DATABASES={
            'default':{
                'ENGINE': 'sqlite3',
                'NAME': 'fakesi_tests.db',
            }
        },
        INSTALLED_APPS=[
            'django.contrib.contenttypes',
            'django.contrib.auth',
            'django.contrib.sessions',
            'fakesi',
            'fakesi.tests',
        ],
        TEMPLATE_CONTEXT_PROCESSORS=[
            'django.core.context_processors.request'
        ],
        TEMPLATE_DIRS=[
            'fakesi/tests/',
        ],
        ROOT_URLCONF='fakesi.tests.urls',
        MIDDLEWARE_CLASSES=[
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'fakesi.middleware.FakesiMiddleware',
        ]
    )

from django.test.simple import run_tests

def runtests(*test_args):
    if not test_args:
        test_args = ['tests']
    parent = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "..",
        "..",
    )
    sys.path.insert(0, parent)
    failures = run_tests(test_args, verbosity=1, interactive=True)
    sys.exit(failures)

if __name__ == '__main__':
    runtests(*sys.argv[1:])